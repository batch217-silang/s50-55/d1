import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
/*[IMPORT THE BOOTSTRAP CSS]*/
import 'bootstrap/dist/css/bootstrap.min.css'
/*[IMPORT THE APPNAVBAR]*/
/*import AppNavbar from './AppNavbar.js'*/

const mongoose = require("mongoose");

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(

  <React.StrictMode>
    <App />
    {/*<AppNavbar/>*/}
  </React.StrictMode>
);


//DB Connection
// mongoose.connect("mongodb+srv://admin:admin123@zuitt.25fvemh.mongodb.net/Course-Booking-React?retryWrites=true&w=majority", {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// });

// //Prompts message in the terminal once connecton is open
// mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))




// const name = "John Smith";
// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user){
//   return user.firstName + " " + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>
// root.render(element);


